<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HeroRepository")
 */
class Hero
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HeroClass")
     * @ORM\JoinColumn(nullable=false)
     */
    private $class;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Rune")
     */
    private $runes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="heroes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->runes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getClass(): ?HeroClass
    {
        return $this->class;
    }

    public function setClass(?HeroClass $class): self
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return Collection|Rune[]
     */
    public function getRunes(): Collection
    {
        return $this->runes;
    }

    public function addRune(Rune $rune): self
    {
        if (!$this->runes->contains($rune)) {
            $this->runes[] = $rune;
        }

        return $this;
    }

    public function removeRune(Rune $rune): self
    {
        if ($this->runes->contains($rune)) {
            $this->runes->removeElement($rune);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
