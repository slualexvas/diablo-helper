<?php

namespace App\Controller;

use App\Entity\Rune;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index()
    {
        return $this->render('main/index.html.twig', [
            'title' => 'Главная страница',
        ]);
    }

    /**
     * @Route("/runes", name="runes")
     */
    public function runes()
    {
        return $this->render('main/runes.html.twig', [
            'title' => 'Руны',
            'runes' => $this->getDoctrine()->getRepository(Rune::class)->findAll()
        ]);
    }
}
