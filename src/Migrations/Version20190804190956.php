<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190804190956 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hero (id INT AUTO_INCREMENT NOT NULL, class_id INT NOT NULL, name VARCHAR(255) NOT NULL, level INT NOT NULL, INDEX IDX_51CE6E86EA000B10 (class_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hero_rune (hero_id INT NOT NULL, rune_id INT NOT NULL, INDEX IDX_F405AC0845B0BCD (hero_id), INDEX IDX_F405AC08E8E5031 (rune_id), PRIMARY KEY(hero_id, rune_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hero ADD CONSTRAINT FK_51CE6E86EA000B10 FOREIGN KEY (class_id) REFERENCES hero_class (id)');
        $this->addSql('ALTER TABLE hero_rune ADD CONSTRAINT FK_F405AC0845B0BCD FOREIGN KEY (hero_id) REFERENCES hero (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hero_rune ADD CONSTRAINT FK_F405AC08E8E5031 FOREIGN KEY (rune_id) REFERENCES rune (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hero_rune DROP FOREIGN KEY FK_F405AC0845B0BCD');
        $this->addSql('DROP TABLE hero');
        $this->addSql('DROP TABLE hero_rune');
    }
}
