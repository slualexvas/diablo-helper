<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Rune;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190512115128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $values = array(
            0  =>
                array(
                    'name'      => 'El',
                    'weapon'    => '+50 To Attack Rating, +1 Light Radius',
                    'armor'     => '+15 Defense, +1 To Light Radius',
                    'req_level' => '11',
                    'recipe'    => '',
                ),
            1  =>
                array(
                    'name'      => 'Eld',
                    'weapon'    => '+75% Damage To Undead, +50 Attack Rating Against Undead',
                    'armor'     => '15% Slower Stamina Drain/7% Increased Chance of Blocking (Shields)',
                    'req_level' => '11',
                    'recipe'    => '3 * El',
                ),
            2  =>
                array(
                    'name'      => 'Tir',
                    'weapon'    => '+2 To Mana After Each Kill',
                    'armor'     => '+2 To Mana After Each Kill',
                    'req_level' => '13',
                    'recipe'    => '3 * Eld',
                ),
            3  =>
                array(
                    'name'      => 'Nef',
                    'weapon'    => 'Knockback',
                    'armor'     => '+30 Defense Vs. Missile',
                    'req_level' => '13',
                    'recipe'    => '3 * Tir',
                ),
            4  =>
                array(
                    'name'      => 'Eth',
                    'weapon'    => '-25% To Target Defense',
                    'armor'     => 'Regenerate Mana 15%',
                    'req_level' => '15',
                    'recipe'    => '3 * Nef',
                ),
            5  =>
                array(
                    'name'      => 'Ith',
                    'weapon'    => '+9 To Maximum Damage',
                    'armor'     => '15% Damage Taken Goes to Mana',
                    'req_level' => '15',
                    'recipe'    => '3 * Eth',
                ),
            6  =>
                array(
                    'name'      => 'Tal',
                    'weapon'    => '+75 Poison Damage Over 5 Seconds',
                    'armor'     => 'Poison Resist 30%/Poison Resist 35% (Shields)',
                    'req_level' => '17',
                    'recipe'    => '3 * Ith',
                ),
            7  =>
                array(
                    'name'      => 'Ral',
                    'weapon'    => 'Adds 5-30 Fire Damage',
                    'armor'     => 'Fire Resist 30%/Fire Resist 35% (Shields)',
                    'req_level' => '19',
                    'recipe'    => '3 * Tal',
                ),
            8  =>
                array(
                    'name'      => 'Ort',
                    'weapon'    => 'Adds 1-50 Lightning Damage',
                    'armor'     => 'Lightning Resist 30%/Lightning Resist 35% (Shields)',
                    'req_level' => '21',
                    'recipe'    => '3 * Ral',
                ),
            9  =>
                array(
                    'name'      => 'Thul',
                    'weapon'    => 'Adds 3-14 Cold Damage - 3 Second Duration',
                    'armor'     => 'Cold Resist 30%/Cold Resist 35% (Shields)',
                    'req_level' => '23',
                    'recipe'    => '3 * Ort',
                ),
            10 =>
                array(
                    'name'      => 'Amn',
                    'weapon'    => '7% Life Stolen Per Hit',
                    'armor'     => 'Attacker Takes Damage of 14',
                    'req_level' => '25',
                    'recipe'    => '3 * Thul + chipped topaz',
                ),
            11 =>
                array(
                    'name'      => 'Sol',
                    'weapon'    => '+9 To Minimum Damage',
                    'armor'     => 'Damage Reduced By 7',
                    'req_level' => '27',
                    'recipe'    => '3 * Amn + chipped amethys',
                ),
            12 =>
                array(
                    'name'      => 'Shael',
                    'weapon'    => '20% Increased Attack Speed',
                    'armor'     => '20% Faster Hit Recovery/20% Faster Block Rate (Shields)',
                    'req_level' => '29',
                    'recipe'    => '3 * Sol + chipped sapphire',
                ),
            13 =>
                array(
                    'name'      => 'Dol',
                    'weapon'    => 'Hit Causes Monster To Flee 25%',
                    'armor'     => 'Replenish Life +7',
                    'req_level' => '31',
                    'recipe'    => '3 * Shael + chipped ruby',
                ),
            14 =>
                array(
                    'name'      => 'Hel',
                    'weapon'    => 'Requirements -20%',
                    'armor'     => 'Requirements -15%',
                    'req_level' => '33',
                    'recipe'    => '3 * Dol + chipped emerald',
                ),
            15 =>
                array(
                    'name'      => 'Io',
                    'weapon'    => '+10 To Vitality',
                    'armor'     => '+10 To Vitality',
                    'req_level' => '35',
                    'recipe'    => '3 * Hel + flawed emerald',
                ),
            16 =>
                array(
                    'name'      => 'Lum',
                    'weapon'    => '+10 To Energy',
                    'armor'     => '+10 To Energy',
                    'req_level' => '37',
                    'recipe'    => '3 * Io + flawed topaz',
                ),
            17 =>
                array(
                    'name'      => 'Ko',
                    'weapon'    => '+10 To Dexterity',
                    'armor'     => '+10 To Dexterity',
                    'req_level' => '39',
                    'recipe'    => '3 * Lum + flawed amethys',
                ),
            18 =>
                array(
                    'name'      => 'Fal',
                    'weapon'    => '+10 To Strength',
                    'armor'     => '+10 To Strength',
                    'req_level' => '41',
                    'recipe'    => '3 * Ko + flawed sapphire',
                ),
            19 =>
                array(
                    'name'      => 'Lem',
                    'weapon'    => '75% Extra Gold From Monsters',
                    'armor'     => '50% Extra Gold From Monsters',
                    'req_level' => '43',
                    'recipe'    => '3 * Fal + flawed ruby',
                ),
            20 =>
                array(
                    'name'      => 'Pul',
                    'weapon'    => '+75% Damage To Demons, +100 Attack Rating Against Demons',
                    'armor'     => '+30% Enhanced Defense',
                    'req_level' => '45',
                    'recipe'    => '3 * Lem + flawed emerald',
                ),
            21 =>
                array(
                    'name'      => 'Um',
                    'weapon'    => '25% Chance of Open Wounds',
                    'armor'     => 'All Resistances +15 (Armor/Helms) +22 (Shields)',
                    'req_level' => '47',
                    'recipe'    => '2 * Pul + flawed emerald',
                ),
            22 =>
                array(
                    'name'      => 'Mal',
                    'weapon'    => 'Prevent Monster Heal',
                    'armor'     => 'Magic Damage Reduced By 7',
                    'req_level' => '49',
                    'recipe'    => '2 * Um + topaz',
                ),
            23 =>
                array(
                    'name'      => 'Ist',
                    'weapon'    => '30% Better Chance of Getting Magic Items',
                    'armor'     => '25% Better Chance of Getting Magic Items',
                    'req_level' => '51',
                    'recipe'    => '2 * Mal + amethys',
                ),
            24 =>
                array(
                    'name'      => 'Gul',
                    'weapon'    => '20% Bonus To Attack Rating',
                    'armor'     => '5% To Maximum Poison Resist',
                    'req_level' => '53',
                    'recipe'    => '2 * Ist + sapphire',
                ),
            25 =>
                array(
                    'name'      => 'Vex',
                    'weapon'    => '7% Mana Stolen Per Hit',
                    'armor'     => '5% To Maximum Fire Resist',
                    'req_level' => '55',
                    'recipe'    => '2 * Gul + ruby',
                ),
            26 =>
                array(
                    'name'      => 'Ohm',
                    'weapon'    => '+50% Enhanced Damage',
                    'armor'     => '5% To Maximum Cold Resist',
                    'req_level' => '57',
                    'recipe'    => '2 * Vex + emerald',
                ),
            27 =>
                array(
                    'name'      => 'Lo',
                    'weapon'    => '20% Deadly Strike',
                    'armor'     => '5% To Maximum Lightning Resist',
                    'req_level' => '59',
                    'recipe'    => '2 * Ohm + emerald',
                ),
            28 =>
                array(
                    'name'      => 'Sur',
                    'weapon'    => 'Hit Blinds Target',
                    'armor'     => 'Maximum Mana 5%/+50 To Mana (Shields)',
                    'req_level' => '61',
                    'recipe'    => '2 * Lo + flawless topaz',
                ),
            29 =>
                array(
                    'name'      => 'Ber',
                    'weapon'    => '20% Chance of Crushing Blow',
                    'armor'     => 'Damage Reduced by 8%',
                    'req_level' => '63',
                    'recipe'    => '2 * Sur + flawless amethys',
                ),
            30 =>
                array(
                    'name'      => 'Jah',
                    'weapon'    => 'Ignore Target\'s Defense',
                    'armor'     => 'Increase Maximum Life 5%/+50 Life (Shields)',
                    'req_level' => '65',
                    'recipe'    => '2 * Ber + flawless sapphire',
                ),
            31 =>
                array(
                    'name'      => 'Cham',
                    'weapon'    => 'Freeze Target +3',
                    'armor'     => 'Cannot Be Frozen',
                    'req_level' => '67',
                    'recipe'    => '2 * Jah + flawless ruby',
                ),
            32 =>
                array(
                    'name'      => 'Zod',
                    'weapon'    => 'Indestructible',
                    'armor'     => 'Indestructible',
                    'req_level' => '69',
                    'recipe'    => '2 * Cham + flawless emerald',
                ),
        );


        $tableName = 'rune';
        foreach ($values as $value) {
            $fieldNames = join(', ', array_keys($value));
            $fieldValues = join(', ', array_map([$this->connection, 'quote'], $value));

            $this->addSql("INSERT INTO {$tableName}({$fieldNames}) VALUES({$fieldValues})");
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE TABLE rune');
    }
}
