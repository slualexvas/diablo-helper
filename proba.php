<?php

$str = '<tr>
      <td><img src="/images/diablo2/rune/El.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">El</td>
      <td><p class="text_t7s">+50 To Attack Rating, +1 Light Radius</td>
      <td><p class="text_t7s">+15 Defense, +1 To Light Radius</td>
      <td><p class="text_t7s">11</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Eld.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Eld</td>
      <td><p class="text_t7s">+75% Damage To Undead, +50 Attack Rating Against Undead</td>
      <td><p class="text_t7s">15% Slower Stamina Drain/7% Increased Chance of Blocking (Shields)</td>
      <td><p class="text_t7s">11</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Tir.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Tir</td>
      <td><p class="text_t7s">+2 To Mana After Each Kill</td>
      <td><p class="text_t7s">+2 To Mana After Each Kill</td>
      <td><p class="text_t7s">13</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Nef.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Nef</td>
      <td><p class="text_t7s">Knockback</td>
      <td><p class="text_t7s">+30 Defense Vs. Missile</td>
      <td><p class="text_t7s">13</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Eth.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Eth</td>
      <td><p class="text_t7s">-25% To Target Defense</td>
      <td><p class="text_t7s">Regenerate Mana 15%</td>
      <td><p class="text_t7s">15</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Ith.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Ith</td>
      <td><p class="text_t7s">+9 To Maximum Damage</td>
      <td><p class="text_t7s">15% Damage Taken Goes to Mana</td>
      <td><p class="text_t7s">15</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Tal.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Tal</td>
      <td><p class="text_t7s">+75 Poison Damage Over 5 Seconds</td>
      <td><p class="text_t7s">Poison Resist 30%/Poison Resist 35% (Shields)</td>
      <td><p class="text_t7s">17</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Ral.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Ral</td>
      <td><p class="text_t7s">Adds 5-30 Fire Damage</td>
      <td><p class="text_t7s">Fire Resist 30%/Fire Resist 35% (Shields)</td>
      <td><p class="text_t7s">19</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Ort.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Ort</td>
      <td><p class="text_t7s">Adds 1-50 Lightning Damage</td>
      <td><p class="text_t7s">Lightning Resist 30%/Lightning Resist 35% (Shields)</td>
      <td><p class="text_t7s">21</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Thul.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Thul</td>
      <td><p class="text_t7s">Adds 3-14 Cold Damage - 3 Second Duration</td>
      <td><p class="text_t7s">Cold Resist 30%/Cold Resist 35% (Shields)</td>
      <td><p class="text_t7s">23</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Amn.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Amn</td>
      <td><p class="text_t7s">7% Life Stolen Per Hit</td>
      <td><p class="text_t7s">Attacker Takes Damage of 14</td>
      <td><p class="text_t7s">25</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Sol.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Sol</td>
      <td><p class="text_t7s">+9 To Minimum Damage</td>
      <td><p class="text_t7s">Damage Reduced By 7</td>
      <td><p class="text_t7s">27</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Shael.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Shael</td>
      <td><p class="text_t7s">20% Increased Attack Speed</td>
      <td><p class="text_t7s">20% Faster Hit Recovery/20% Faster Block Rate (Shields)</td>
      <td><p class="text_t7s">29</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Dol.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Dol</td>
      <td><p class="text_t7s">Hit Causes Monster To Flee 25%</td>
      <td><p class="text_t7s">Replenish Life +7</td>
      <td><p class="text_t7s">31</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Hel.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Hel</td>
      <td><p class="text_t7s">Requirements -20%</td>
      <td><p class="text_t7s">Requirements -15%</td>
      <td><p class="text_t7s">33</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Io.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Io</td>
      <td><p class="text_t7s">+10 To Vitality</td>
      <td><p class="text_t7s">+10 To Vitality</td>
      <td><p class="text_t7s">35</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Lum.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Lum</td>
      <td><p class="text_t7s">+10 To Energy</td>
      <td><p class="text_t7s">+10 To Energy</td>
      <td><p class="text_t7s">37</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Ko.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Ko</td>
      <td><p class="text_t7s">+10 To Dexterity</td>
      <td><p class="text_t7s">+10 To Dexterity</td>
      <td><p class="text_t7s">39</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Fal.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Fal</td>
      <td><p class="text_t7s">+10 To Strength</td>
      <td><p class="text_t7s">+10 To Strength</td>
      <td><p class="text_t7s">41</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Lem.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Lem</td>
      <td><p class="text_t7s">75% Extra Gold From Monsters</td>
      <td><p class="text_t7s">50% Extra Gold From Monsters</td>
      <td><p class="text_t7s">43</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Pul.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Pul</td>
      <td><p class="text_t7s">+75% Damage To Demons, +100 Attack Rating Against Demons</td>
      <td><p class="text_t7s">+30% Enhanced Defense</td>
      <td><p class="text_t7s">45</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Um.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Um</td>
      <td><p class="text_t7s">25% Chance of Open Wounds</td>
      <td><p class="text_t7s">All Resistances +15 (Armor/Helms) +22 (Shields)</td>
      <td><p class="text_t7s">47</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Mal.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Mal</td>
      <td><p class="text_t7s">Prevent Monster Heal</td>
      <td><p class="text_t7s">Magic Damage Reduced By 7</td>
      <td><p class="text_t7s">49</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Ist.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Ist</td>
      <td><p class="text_t7s">30% Better Chance of Getting Magic Items</td>
      <td><p class="text_t7s">25% Better Chance of Getting Magic Items</td>
      <td><p class="text_t7s">51</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Gul.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Gul</td>
      <td><p class="text_t7s">20% Bonus To Attack Rating</td>
      <td><p class="text_t7s">5% To Maximum Poison Resist</td>
      <td><p class="text_t7s">53</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Vex.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Vex</td>
      <td><p class="text_t7s">7% Mana Stolen Per Hit</td>
      <td><p class="text_t7s">5% To Maximum Fire Resist</td>
      <td><p class="text_t7s">55</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Ohm.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Ohm</td>
      <td><p class="text_t7s">+50% Enhanced Damage</td>
      <td><p class="text_t7s">5% To Maximum Cold Resist</td>
      <td><p class="text_t7s">57</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Lo.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Lo</td>
      <td><p class="text_t7s">20% Deadly Strike</td>
      <td><p class="text_t7s">5% To Maximum Lightning Resist</td>
      <td><p class="text_t7s">59</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Sur.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Sur</td>
      <td><p class="text_t7s">Hit Blinds Target</td>
      <td><p class="text_t7s">Maximum Mana 5%/+50 To Mana (Shields)</td>
      <td><p class="text_t7s">61</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Ber.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Ber</td>
      <td><p class="text_t7s">20% Chance of Crushing Blow</td>
      <td><p class="text_t7s">Damage Reduced by 8%</td>
      <td><p class="text_t7s">63</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Jah.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Jah</td>
      <td><p class="text_t7s">Ignore Target\'s Defense</td>
      <td><p class="text_t7s">Increase Maximum Life 5%/+50 Life (Shields)</td>
      <td><p class="text_t7s">65</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Cham.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Cham</td>
      <td><p class="text_t7s">Freeze Target +3</td>
      <td><p class="text_t7s">Cannot Be Frozen</td>
      <td><p class="text_t7s">67</td>
    </tr>
    <tr>
      <td><img src="/images/diablo2/rune/Zod.gif" width="28" height="28" alt="" style="margin:5px;" /></td>
      <td><p class="text_t7s">Zod</td>
      <td><p class="text_t7s">Indestructible</td>
      <td><p class="text_t7s">Indestructible</td>
      <td><p class="text_t7s">69</td>
    </tr>';

$dom = new DOMDocument();
$dom->loadHTML($str);
$trs = $dom->getElementsByTagName('tr');

$data = [];
$prevName = '';
foreach($trs as $tr) {
    $text = $tr->textContent;
    $arr = [];
    foreach (explode("\n", $text) as $item) {
        $item = trim($item);
        if (!empty($item)) {
            $arr[] = $item;
        }
    }
    // name, req_level, weapon, armor, recipe
    $arr = [
        'name' => $arr[0],
        'weapon' => $arr[1],
        'armor' => $arr[2],
        'req_level' => $arr[3],
        'recipe' => recipe($prevName, $arr[3])
    ];

    $data[] = $arr;
    $prevName = $arr['name'];
}

var_export($data);

function recipe(string $prevName, int $runeLevel)
{
    if (empty($prevName)) {
        return '';
    }

    $count = $runeLevel <= 45 ? 3 : 2;

    $result = "{$count} * {$prevName}";

    if ($runeLevel <= 23) {
        $gemLevel = '';
    } elseif ($runeLevel <= 33) {
        $gemLevel = 'chipped';
    } elseif ($runeLevel <= 47) {
        $gemLevel = 'flawed';
    } elseif ($runeLevel <= 59) {
        $gemLevel = '';
    } else {
        $gemLevel = 'flawless';
    }

    if ($runeLevel <= 23) {
        $gem = '';
    } elseif (in_array($runeLevel, [25, 37, 49, 61])) {
        $gem = 'topaz';
    } elseif (in_array($runeLevel, [27, 39, 51, 63])) {
        $gem = 'amethys';
    } elseif (in_array($runeLevel, [29, 41, 53, 65])) {
        $gem = 'sapphire';
    } elseif (in_array($runeLevel, [31, 43, 55, 67])) {
        $gem = 'ruby';
    } elseif (in_array($runeLevel, [33, 45, 57, 69])) {
        $gem = 'emerald';
    } elseif (in_array($runeLevel, [35, 47, 59])) {
        $gem = 'emerald';
    } else {
        throw new Exception("unknown gem");
    }

    if (!empty($gem)) {
        $result .= " + {$gemLevel} {$gem}";
    }

    $result = str_replace('  ', ' ', $result);
    return trim($result);
}
